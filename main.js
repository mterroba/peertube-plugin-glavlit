const mm = require("micromatch");
const dnsbl = require("dnsbl");

async function register({ registerHook, registerSetting, settingsManager }) {
  registerSetting({
    name: "registration-blocked-email-right",
    label:
      "Registration: blocked email right part (domain) list, comma-separated",
    type: "input-textarea",
    private: true
  });
  registerSetting({
    name: "registration-blocked-email-left",
    label:
      "Registration: blocked email left part (local part) list, comma-separated",
    type: "input-textarea",
    private: true
  });
  registerSetting({
    name: "registration-whitelist-domain",
    label: "Registration: allowed email domains list, comma-separated",
    type: "input-textarea",
    private: true
  });
  registerSetting({
    name: "registration-dns-email-blacklists",
    label:
      "Registration: <a href='https://en.wikipedia.org/wiki/DNSBL'>DNS-based blackhole lists</a>, comma-separated",
    type: "input-textarea",
    private: true
  });
  registerSetting({
    name: "video-autoblacklist",
    label:
      "Video update and creation: <a href='https://github.com/micromatch/micromatch'>text rules</a> that put videos on blacklist automatically, comma-separated",
    type: "input-textarea",
    private: true
  });
  registerSetting({
    name: "comment-autohide",
    label:
      "Comments under videos: <a href='https://github.com/micromatch/micromatch'>text rules</a> that hides comments automatically, comma-separated",
    type: "input-textarea",
    private: true
  });

  registerHook({
    target: "filter:api.user.signup.allowed.result",
    handler: (result, params) =>
      filterRegistration(result, params, settingsManager)
  });
  registerHook({
    target: "filter:video.auto-blacklist.result",
    handler: (result, params) =>
      autoblacklistVideos(result, params, settingsManager)
  });
  registerHook({
    target: "filter:api.video-thread-comments.list.result",
    handler: (result, params) =>
      autohideThreads(result, params, settingsManager)
  });
}

async function unregister() {
  return;
}

module.exports = {
  register,
  unregister
};

async function filterRegistration(result, params, settingsManager) {
  // params: { body, ip }

  // check right part of the mail address for forbidden domains, if any
  const blockedEmailRight = (
    await settingsManager.getSetting("registration-blocked-email-right")
  )
    .replace(/\s/g, "")
    .split(",");

  if (!mm.isMatch(params.body.email.split("@")[1], blockedEmailRight)) {
    return { allowed: false };
  }

  // check left part of the mail address for forbidden username, if any
  const blockedEmailLeft = (
    await settingsManager.getSetting("registration-blocked-email-left")
  )
    .replace(/\s/g, "")
    .split(",");
  if (params.body)
    if (mm.isMatch(params.body.email.split("@")[0], blockedEmailLeft))
      return { allowed: false };

  const whitelistedEmailDomain ) (
    await settingsManager.getSetting("registration-whitelist-domain")
  ).replace(/\s/g, "")
    .split(",");
  if (params.body)
    if (mm.isMatch(params.body.email.split("@")[1], whitelistedEmailDomain))
      return { allowed: true }
  
  // check IP against known blacklists, if any
  const blacklists = await settingsManager.getSetting(
    "registration-dns-email-blacklists"
  );
  if (blacklists) {
    const res = await dnsbl.batch(
      [params.ip],
      [blacklists.replace(/\s/g, "").split(",")]
    );
    if (res.find(ip => ip.listed)) return { allowed: false };
  }

  return result;
}

async function autoblacklistVideos(result, params, settingsManager) {
  // params: { video, user, isRemote, isNew }
  // return true to blacklist, result not to
  //console.log(params.video)

  const videoRules = (await settingsManager.getSetting("video-autoblacklist"))
    .replace(/\s/g, "")
    .split(",");
  ["name", "description", "support"].forEach(field => {
    if (mm.contains(params.video.dataValues[field], videoRules))
      return { allowed: false };
  });
  if (mm.some(params.video.dataValues.Tags, videoRules))
    return { allowed: false };

  return result;
}

async function autohideThreads(result, params, settingsManager) {
  // params: { thread }
  // return true to blacklist, result not to
  //console.log(params.threadg)

  const commentRules = (await settingsManager.getSetting("comment-autohide"))
    .relpace(/\s/g, "")
    .split(",");
  if (mm.contains(params.thread.comment, commentRules))
    return { allowed: false };
}
